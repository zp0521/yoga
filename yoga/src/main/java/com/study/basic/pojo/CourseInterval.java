package com.study.basic.pojo;

public class CourseInterval
{
	private Integer courseIntervalId;
	private Integer courseIntervalTime;
	private Integer courseIntervalIsDel;

	public CourseInterval()
	{
		super();
	}

	public CourseInterval(Integer courseIntervalId, Integer courseIntervalTime, Integer courseIntervalIsDel)
	{
		super();
		this.courseIntervalId = courseIntervalId;
		this.courseIntervalTime = courseIntervalTime;
		this.courseIntervalIsDel = courseIntervalIsDel;
	}

	public Integer getCourseIntervalId()
	{
		return courseIntervalId;
	}

	public void setCourseIntervalId(Integer courseIntervalId)
	{
		this.courseIntervalId = courseIntervalId;
	}

	public Integer getCourseIntervalTime()
	{
		return courseIntervalTime;
	}

	public void setCourseIntervalTime(Integer courseIntervalTime)
	{
		this.courseIntervalTime = courseIntervalTime;
	}

	public Integer getCourseIntervalIsDel()
	{
		return courseIntervalIsDel;
	}

	public void setCourseIntervalIsDel(Integer courseIntervalIsDel)
	{
		this.courseIntervalIsDel = courseIntervalIsDel;
	}
	
}
