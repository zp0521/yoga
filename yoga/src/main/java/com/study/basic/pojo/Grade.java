package com.study.basic.pojo;

/*gradeID int not null primary key auto_increment, 
gradeCode char(5), 权限ID
gradeCost int*/

public class Grade
{
	private Integer gradeId;
	private String gradeCode;
	private Integer gradeCost;
	private Integer gradeIsDel;
	public Grade()
	{
		super();
		// TODO Auto-generated constructor stub
	}
	public Grade(Integer gradeId, String gradeCode, Integer gradeCost, Integer gradeIsDel)
	{
		super();
		this.gradeId = gradeId;
		this.gradeCode = gradeCode;
		this.gradeCost = gradeCost;
		this.gradeIsDel = gradeIsDel;
	}
	public Integer getGradeId()
	{
		return gradeId;
	}
	public void setGradeId(Integer gradeId)
	{
		this.gradeId = gradeId;
	}
	public String getGradeCode()
	{
		return gradeCode;
	}
	public void setGradeCode(String gradeCode)
	{
		this.gradeCode = gradeCode;
	}
	public Integer getGradeCost()
	{
		return gradeCost;
	}
	public void setGradeCost(Integer gradeCost)
	{
		this.gradeCost = gradeCost;
	}
	public Integer getGradeIsDel()
	{
		return gradeIsDel;
	}
	public void setGradeIsDel(Integer gradeIsDel)
	{
		this.gradeIsDel = gradeIsDel;
	}
	@Override
	public String toString()
	{
		return "Grade [gradeId=" + gradeId + ", gradeCode=" + gradeCode + ", gradeCost=" + gradeCost + ", gradeIsDel="
				+ gradeIsDel + "]";
	}
}
