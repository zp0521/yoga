package com.study.basic.service;

import java.util.List;

import com.study.basic.pojo.CourseInterval;
import com.study.basic.pojo.Grade;
import com.study.util.page.PageInfo;

public interface BasicService
{
	List<Grade> queryGrade( PageInfo pageInfo);
	Grade queryOneGrade(Integer gradeId);
	Grade addGrade(Grade grade);
	boolean updateGradeStatus(Grade grade);
	Grade updateGrade(Grade grade);
	
	List<CourseInterval> queryCourseInterval();
	CourseInterval queryOneCourseInterval(Integer courseIntervalId);
	CourseInterval updateCourseInterval(CourseInterval courseInterval);
	CourseInterval addCourseInterval(CourseInterval courseInterval);
}
