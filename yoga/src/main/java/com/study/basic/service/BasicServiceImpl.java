package com.study.basic.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.study.basic.dao.CourseIntervalDao;
import com.study.basic.dao.GradeDao;
import com.study.basic.pojo.CourseInterval;
import com.study.basic.pojo.Grade;
import com.study.util.page.PageInfo;

/**
 * 有事务,如增加、修改、删除.在方法前用这个注解
 * @Transactional(propagation=Propagation.REQUIRED,isolation=Isolation.DEFAULT,rollbackFor=Exception
 * .class) 
 * 没有事务,如查询,在方法前用这个注解
 * @Transactional(propagation=Propagation.SUPPORTS,readOnly=true) 
 * @author: 满脑子代码的祝大朋
 * @date: 2019年1月13日 上午10:46:10
 */
@Service
public class BasicServiceImpl implements BasicService
{
	@Autowired
	@Qualifier("gradeDao")
	private GradeDao gradeDao;
	@Autowired
	@Qualifier("courseIntervalDao")
	private CourseIntervalDao courseIntervalDao;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Grade> queryGrade(PageInfo pageInfo)
	{
		return gradeDao.listPageGrade(pageInfo);
	}
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Grade queryOneGrade(Integer gradeId)
	{
		return gradeDao.queryOneGrade(gradeId);
	}
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, rollbackFor = Exception.class)
	public Grade addGrade(Grade grade)
	{
		Grade returngrade = null;
		boolean b = gradeDao.addGrade(grade);
		if (b)
		{
			returngrade = gradeDao.queryOneGrade(grade.getGradeId());
		}
		return returngrade;
	}
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, rollbackFor = Exception.class)
	public Grade updateGrade(Grade grade)
	{
		Grade returngrade = null;
		boolean b = gradeDao.updateGrade(grade);
		if (b)
		{
			returngrade = gradeDao.queryOneGrade(grade.getGradeId());
		}
		return returngrade;
	}
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, rollbackFor = Exception.class)
	public boolean updateGradeStatus(Grade grade)
	{
		return gradeDao.updateGradeStatus(grade);
	}
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<CourseInterval> queryCourseInterval()
	{
		return courseIntervalDao.queryCourseInterval();
	}
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public CourseInterval queryOneCourseInterval(Integer courseIntervalId)
	{
		return courseIntervalDao.queryOneCourseInterval(courseIntervalId);
	}
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, rollbackFor = Exception.class)
	public CourseInterval updateCourseInterval(CourseInterval courseInterval)
	{
		CourseInterval returnCourseInterval = null;
		boolean rt = courseIntervalDao.updateCourseInterval(courseInterval);
		if (rt)
		{
			returnCourseInterval = courseIntervalDao.queryOneCourseInterval(courseInterval.getCourseIntervalId());
		}
		return returnCourseInterval;
	}
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, rollbackFor = Exception.class)
	public CourseInterval addCourseInterval(CourseInterval courseInterval)
	{
		CourseInterval returnCourseInterval = null;
		boolean rt = courseIntervalDao.addCourseInterval(courseInterval);
		if (rt)
		{
			returnCourseInterval = courseIntervalDao.queryOneCourseInterval(courseInterval.getCourseIntervalId());
		}
		return returnCourseInterval;
	}
}
