package com.study.basic.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.study.basic.pojo.CourseInterval;
import com.study.basic.pojo.Grade;
import com.study.basic.service.BasicService;
import com.study.util.page.PageInfo;

@Controller
@RequestMapping("/basic")
public class BasicController
{
	@Autowired
	@Qualifier("basicServiceImpl")
	private BasicService basicService;
	
	@RequestMapping(value="/showgrade",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView showGrade(PageInfo pageInfo)throws Exception
	{
		ModelAndView mv = new ModelAndView();
		List<Grade> gradeList = basicService.queryGrade(pageInfo);
		//System.out.println(gradeList);
		mv.addObject("gradeList", gradeList);
		mv.setViewName("/grademain");
		return mv;
	}
	@RequestMapping(value="/delgrade",method={RequestMethod.POST})
	public @ResponseBody boolean delGrade(@RequestBody Grade grade)throws Exception
	{
		return basicService.updateGradeStatus(grade);
		
	}
	@RequestMapping(value="/editgrade",method={RequestMethod.POST})
	public @ResponseBody Grade editGrade(Integer gradeId)throws Exception
	{
		return basicService.queryOneGrade(gradeId);
	}
	
	@RequestMapping(value="/savegrade",method={RequestMethod.POST})
	public @ResponseBody Grade saveGrade(Grade grade)throws Exception
	{
		if(grade.getGradeId()!=null)
		{
			return basicService.updateGrade(grade);
		}
		else
		{
			return basicService.addGrade(grade);
			
		}
	}
	
	//时间间隔
	@RequestMapping(value="/showcourseInterval",method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView showCourseInterval()throws Exception
	{
		ModelAndView mv = new ModelAndView();
		List<CourseInterval>  courseIntervalList= basicService.queryCourseInterval();
		//System.out.println(gradeList);
		mv.addObject("courseIntervalList", courseIntervalList);
		mv.setViewName("courserintervalmain");
		return mv;
	}
	@RequestMapping(value="/delcourseinterval",method={RequestMethod.POST})
	public @ResponseBody boolean delCourseInterval(@RequestBody CourseInterval courseInterval)throws Exception
	{
		return basicService.updateCourseInterval(courseInterval) != null;
		
	}
	
	@RequestMapping(value="/editcourseinterval",method={RequestMethod.POST})
	public @ResponseBody CourseInterval editCourseInterval(Integer courseIntervalId)throws Exception
	{
		return basicService.queryOneCourseInterval(courseIntervalId);
	}
	
	
	@RequestMapping(value="/savecourseinterval",method={RequestMethod.POST})
	public @ResponseBody CourseInterval saveCourseInterval(CourseInterval courseInterval)throws Exception
	{
		if(courseInterval.getCourseIntervalId()!=null)
		{
			return basicService.updateCourseInterval(courseInterval);
		}
		else
		{
			return basicService.addCourseInterval(courseInterval);
			
		}
	}
}

