package com.study.basic.dao;

import java.util.List;

import com.study.basic.pojo.CourseInterval;

public interface CourseIntervalDao
{
	List<CourseInterval> queryCourseInterval();
	CourseInterval queryOneCourseInterval(Integer courseIntervalId);
	boolean updateCourseInterval(CourseInterval courseInterval);
	boolean addCourseInterval(CourseInterval courseInterval);
}
