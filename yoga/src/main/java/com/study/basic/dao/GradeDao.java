package com.study.basic.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.study.basic.pojo.Grade;
import com.study.util.page.PageInfo;

public interface GradeDao
{
	List<Grade> listPageGrade(@Param("page")PageInfo pageInfo);
	Grade queryOneGrade(Integer gradeId);
	boolean addGrade(Grade grade);
	boolean updateGrade(Grade grade);
	boolean updateGradeStatus(Grade grade);
}
